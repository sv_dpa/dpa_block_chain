from flask import Flask, render_template, request, session, jsonify
from flask_socketio import SocketIO, emit, send

app = Flask(__name__)
socketio = SocketIO(app, manage_session=False)
clients = []


@socketio.on("client_connected", namespace="/connect")
def handle_client_connected_event(username, methods=["GET", "POST"]):
    clients.append(
        {
            "client_session": request.sid,
            "client_username": username,
            "client_ip": request.access_route,
        }
    )
    print(clients)


@socketio.on("disconnect")
def socket_disconnect():
    clients.remove(request.namespace)
    print("Client disconnected")


@app.route("/")
def index(methods=["GET", "POST"]):
    print("Hi", clients)
    return render_template("index.html")


if __name__ == "__main__":
    socketio.run(app, debug=True, host="192.168.0.105")

