from flask import (
    render_template,
    request,
    session,
    flash,
    redirect,
    url_for,
    request,
    Blueprint,
)
from flask_login import (
    current_user,
    login_user,
    logout_user,
    login_required,
)

from app import db, bcrypt


from .service import save_picture
from .forms import RegistrationForm, LoginForm, UpdateAccountForm
from .models import User

users = Blueprint('users', __name__)


@users.route("/user/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("main.home"))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_pwd = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
        user = User(
            username=form.username.data, email=form.username.data, password=hashed_pwd,
        )
        db.session.add(user)
        db.session.commit()
        flash(f"Account created for {form.username.data}", "success")
        return redirect(url_for("users.login"))
    return render_template("register.html", title="Register", form=form)


@users.route("/user/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("main.home"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data,).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get("next")
            flash(f"Logged IN", "success")
            return redirect(next_page) if next_page else redirect(url_for("main.home"))
        else:
            flash(f"Login Unsuccessful", "danger")
    return render_template("login.html", title="Login", form=form)


@users.route("/user/logout")
def logout():
    logout_user()
    return redirect(url_for("main.home"))


@users.route("/user/account", methods=["GET", "POST"])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account',
                           image_file=image_file, form=form)
