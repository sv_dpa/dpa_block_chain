from flask_login import LoginManager, current_user, UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

import base64
import datetime
import time
import hashlib

DIFFICULTY = 2
MINE_RATE = 3000

class Block(object):
    
    def __init__(self, timestamp, lasthash, data, nonce, difficulty=DIFFICULTY):
        self.timestamp = timestamp
        self.lastHash = lasthash
        self.data = data
        self.nonce = nonce
        self.difficulty = difficulty
        self.hash = Block.generate_hash(self.timestamp, self.lastHash, self.data, self.nonce, self.difficulty)
    
    def __str__(self):
        return f'\
            TimeStamp:......{self.timestamp}\n\
            Last Hash:......{self.lastHash}\n\
            Hash:...........{self.hash}\n\
            Nonce:..........{self.nonce}\n\
            Difficulty:.....{self.difficulty}\n\
            Data:...........{self.data}\n'

    @staticmethod
    def generate_hash(timestamp, lastHash, data, nonce, difficulty):
        key = hashlib.sha256()
        key.update(str(timestamp).encode('utf-8'))
        key.update(str(lastHash).encode('utf-8'))
        key.update(base64.b64encode(str(data).encode('utf-8')))
        key.update(str(nonce).encode('utf-8'))
        key.update(str(difficulty).encode('utf-8'))
        return key.hexdigest()

    @staticmethod
    def genesis():
        return Block(round(time.time() * 1000), None, [], 0)
    
    @staticmethod
    def adjust_difficulty(lastBlock, currenttime):
        difficulty = lastBlock.difficulty
        if lastBlock.timestamp + MINE_RATE > currenttime:
            return difficulty+1 
        else:
            return difficulty-1

    @staticmethod
    def mine_block(lastBlock, data):
        lastHash = lastBlock.hash
        difficulty = lastBlock.difficulty
        nonce = 0

        while True:
            nonce +=1
            timestamp = round(time.time() * 1000)
            difficulty = Block.adjust_difficulty(lastBlock, timestamp)
            hash = Block.generate_hash(timestamp, lastHash, data, nonce, difficulty)

            if hash[0:difficulty] == '0'*difficulty:
                break
        
        return Block(timestamp, lastHash, data, nonce, difficulty)


class BlockChain(object):

    def __init__(self):
        self.chain = [Block.genesis()]
    
    def add_block(self, data):
        block = Block.mine_block(self.chain[-1], data)
        self.chain.append(block)
        return block

    def get_block(self, index):
        return self.chain[index]
    